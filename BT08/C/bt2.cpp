#include <iostream>
#include <cstring>

using namespace std;

const int N=10000;

void add(char s[6][N],char a )
{
    if (a=='A')
    {
        strcat(s[0],"     #      ");
        strcat(s[1],"    # #     ");
        strcat(s[2],"   #####    ");
        strcat(s[3],"  #     #   ");
        strcat(s[4]," #       #  ");
        strcat(s[5],"#         # ");
    }
    if (a=='B')
    {
        strcat(s[0],"######  ");
        strcat(s[1],"#     # ");
        strcat(s[2],"######  ");
        strcat(s[3],"#     # ");
        strcat(s[4],"#     # ");
        strcat(s[5],"######  ");
    }
    if (a=='C')
    {
        strcat(s[0]," ###### ");
        strcat(s[1],"#       ");
        strcat(s[2],"#       ");
        strcat(s[3],"#       ");
        strcat(s[4],"#       ");
        strcat(s[5]," ###### ");
    }
        if (a=='D')
    {
        strcat(s[0],"#####   ");
        strcat(s[1],"#    #  ");
        strcat(s[2],"#     # ");
        strcat(s[3],"#     # ");
        strcat(s[4],"#    #  ");
        strcat(s[5],"#####   ");
    }
        if (a=='E')
    {
        strcat(s[0],"####### ");
        strcat(s[1],"#       ");
        strcat(s[2],"######  ");
        strcat(s[3],"#       ");
        strcat(s[4],"#       ");
        strcat(s[5],"####### ");
    }
        if (a=='F')
    {
        strcat(s[0],"####### ");
        strcat(s[1],"#       ");
        strcat(s[2],"######  ");
        strcat(s[3],"#       ");
        strcat(s[4],"#       ");
        strcat(s[5],"#       ");
    }
        if (a=='G')
    {
        strcat(s[0]," #####  ");
        strcat(s[1],"#     # ");
        strcat(s[2],"#       ");
        strcat(s[3],"# ##### ");
        strcat(s[4],"#     # ");
        strcat(s[5]," #####  ");
    }
        if (a=='H')
    {
        strcat(s[0],"#     # ");
        strcat(s[1],"#     # ");
        strcat(s[2],"####### ");
        strcat(s[3],"#     # ");
        strcat(s[4],"#     # ");
        strcat(s[5],"#     # ");
    }
        if (a=='I')
    {
        strcat(s[0],"### ");
        strcat(s[1]," #  ");
        strcat(s[2]," #  ");
        strcat(s[3]," #  ");
        strcat(s[4]," #  ");
        strcat(s[5],"### ");
    }
        if (a=='J')
    {
        strcat(s[0],"   # ");
        strcat(s[1],"   # ");
        strcat(s[2],"   # ");
        strcat(s[3],"   # ");
        strcat(s[4],"#  # ");
        strcat(s[5]," ##  ");
    }
        if (a=='K')
    {
        strcat(s[0],"#   #  ");
        strcat(s[1],"#  #   ");
        strcat(s[2],"###    ");
        strcat(s[3],"#  #   ");
        strcat(s[4],"#   #  ");
        strcat(s[5],"#    # ");
    }
    if (a=='L')
    {
        strcat(s[0],"#     ");
        strcat(s[1],"#     ");
        strcat(s[2],"#     ");
        strcat(s[3],"#     ");
        strcat(s[4],"#     ");
        strcat(s[5],"##### ");
    }
        if (a=='M')
    {
        strcat(s[0],"#    # ");
        strcat(s[1],"##  ## ");
        strcat(s[2],"# ## # ");
        strcat(s[3],"#    # ");
        strcat(s[4],"#    # ");
        strcat(s[5],"#    # ");
    }
        if (a=='N')
    {
        strcat(s[0],"#    # ");
        strcat(s[1],"##   # ");
        strcat(s[2],"# #  # ");
        strcat(s[3],"#  # # ");
        strcat(s[4],"#   ## ");
        strcat(s[5],"#    # ");
    }
        if (a=='O')
    {
        strcat(s[0]," #####  ");
        strcat(s[1],"#     # ");
        strcat(s[2],"#     # ");
        strcat(s[3],"#     # ");
        strcat(s[4],"#     # ");
        strcat(s[5]," #####  ");
    }
        if (a=='P')
    {
        strcat(s[0],"######  ");
        strcat(s[1],"#     # ");
        strcat(s[2],"######  ");
        strcat(s[3],"#       ");
        strcat(s[4],"#       ");
        strcat(s[5],"#       ");
    }
        if (a=='Q')
    {
        strcat(s[0],"  ####   ");
        strcat(s[1]," #    #  ");
        strcat(s[2],"#      # ");
        strcat(s[3],"#    # # ");
        strcat(s[4]," #    #  ");
        strcat(s[5],"  #### # ");
    }
        if (a=='R')
    {
        strcat(s[0],"#####   ");
        strcat(s[1],"#    #  ");
        strcat(s[2],"#####   ");
        strcat(s[3],"#   #   ");
        strcat(s[4],"#    #  ");
        strcat(s[5],"#     # ");
    }
        if (a=='S')
    {
        strcat(s[0]," #####  ");
        strcat(s[1],"#       ");
        strcat(s[2]," #####  ");
        strcat(s[3],"      # ");
        strcat(s[4],"#     # ");
        strcat(s[5]," #####  ");
    }
        if (a=='T')
    {
        strcat(s[0],"####### ");
        strcat(s[1],"   #    ");
        strcat(s[2],"   #    ");
        strcat(s[3],"   #    ");
        strcat(s[4],"   #    ");
        strcat(s[5],"   #    ");
    }
        if (a=='U')
    {
        strcat(s[0],"#     # ");
        strcat(s[1],"#     # ");
        strcat(s[2],"#     # ");
        strcat(s[3],"#     # ");
        strcat(s[4],"#     # ");
        strcat(s[5]," #####  ");
    }
        if (a=='V')
    {
        strcat(s[0],"#    # ");
        strcat(s[1],"#    # ");
        strcat(s[2],"#    # ");
        strcat(s[3],"#    # ");
        strcat(s[4]," #  #  ");
        strcat(s[5],"  ##   ");
    }
        if (a=='W')
    {
        strcat(s[0],"#     # ");
        strcat(s[1],"#     # ");
        strcat(s[2],"#  #  # ");
        strcat(s[3],"# # # # ");
        strcat(s[4],"# # # # ");
        strcat(s[5]," #   #  ");
    }
        if (a=='X')
    {
        strcat(s[0]," #   #  ");
        strcat(s[1],"  # #   ");
        strcat(s[2],"   #    ");
        strcat(s[3],"  # #   ");
        strcat(s[4]," #   #  ");
        strcat(s[5],"#     # ");
    }
        if (a=='Y')
    {
        strcat(s[0],"#     # ");
        strcat(s[1]," #   #  ");
        strcat(s[2],"  # #   ");
        strcat(s[3],"   #    ");
        strcat(s[4],"   #    ");
        strcat(s[5],"   #    ");
    }
        if (a=='Z')
    {
        strcat(s[0],"###### ");
        strcat(s[1],"    #  ");
        strcat(s[2],"   #   ");
        strcat(s[3],"  #    ");
        strcat(s[4]," #     ");
        strcat(s[5],"###### ");
    }


}

int main()
{
    char a[N], s[6][N];
    for(int i=0;i<6;i++)
        s[i][0]='\0';
    cin >> a;
    int n=strlen(a);
    for(int i=0;i<n;i++)
    {
        add(s,toupper(a[i]));
    }
    for(int i=0;i<6;i++)
        cout<<s[i]<<endl;
}
